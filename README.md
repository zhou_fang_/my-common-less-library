
## 3. Common less library:
=========================

###File structure:
```

less
│─ compass.less                            <= for upano compass only
│
└─lib-share                                <= common classes
    │  animation.less <= animation classes
    │  box-elevation-shadow.less
    │  box-shadow.less
    │  common.less
    │  input.less
    │  perfect-scroll-bar.less
    │
    ├─loading
    │      logo-dot.less
    │
    └─rulesets-mixin                      <----- mixin only
            animation.ruleset.less
            box-elevation-shadow.mixin.less
            box-shadow.mixin.less
            responsive.mixin.less
```
### Version 1.0.5
- Add flash button
- fix: screen-customize mixin

### Version 1.0.0
- Add documentation (comment) for animation duration 
- Add zf-input class

#
#
#
## This Libary Include:
###   responsive mixin (1em = 16px)
    - sm: 0~40em;
    - md: 40em ~ 64em;
    - lg: 64em ~ 75em;
    - xl: 75em ~ 90em;
    - xxl: 90em up
    ---
    Example:
    +  sm and smaller :
    .screen-sm-max(@rules)
    + md and larger:
      .screen-md-min(@rules)
    + md only:
      .screen-md(@rules)
    +  md and smaller: 
      .screen-md-max(@rules)
    + Same as lg xl and xxl

---

###   Responsive animation mixin (erasing duration, base on the material design).
 Basic concept:
   - Transitions on mobile typically occur over 300ms, within this margin of variance:
   - Large, complex, full-screen transitions may have longer durations, occurring over 375ms
   - Elements entering the screen occur over 225ms
   - Elements leaving the screen occur over 195ms
   - Transitions that exceed 400ms may feel too slow.
   - Durations on tablet should be about 30% longer than on mobile.
   - Desktop animations should be faster and simpler than their mobile counterparts. These animations should last 150ms to 200ms.

   | mixin                 |     sm screen | md         | lg       |
   | --------------------- |:-------------:| ----------:|---------:|
   | .duration_r(normal)   | 300ms         | 300ms *1.3 | 200ms    |
   | .duration_r(in)       | 225ms         |  225ms*1.3 | 175ms    |
   | .duration_r(out)      | 195ms         | 195ms *1.3 | 150ms    |
   | .duration_r(complex)  | 375ms         |    375*1.3 | 500ms    |



##### Pre defined animation curve.  
   -  @Standard_curve:  Entering or exiting objects that move other on-screen elements do so along a smooth easing curve,for example: change the size.
    -  @Easing_in: Deceleration curve: Elements entering the screen use the deceleration curve
   -  @Easing_out: Acceleration curve: For permanently leaving the screen for example alert box
   -  @Sharp_curve:  Sharp curve: Temporarily leaving the screen.
* for more detail please read the code.
   
####  Box-shadow mixin and class (.box-shadow1 to .box-shadow4).check the index.html to preview the result
####   Elevation-z (it's also box-shadow, base on the material design, level form 0~24)
    check the index.html to preview the result

#### Customize input styles for angular form module. 
class include:
- error message, 
- ng-invalid.ng-touched
- [required]
- invalid
- option: disabled
- and more
 **Check the index.html for demo.**

#### perfect-scroll-bar.less 
a customize style for perfect-scroll-bar https://github.com/utatti/perfect-scrollbar
replace the default css file.

##### Others

Some other animations, keyframe for personal use.
